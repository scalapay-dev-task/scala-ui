import reducer, { addElement, changeQuantity, removeElement } from '../src/entities/basket';

describe('Basket store', () =>
{
	it('should return the initial state', () => {
		expect(reducer(undefined, {})).toEqual({
			value: []
		})
	})

	it('should handle a product being added to an empty list', () => {
		const previousState = {
			value: [],
		}

		const productToAdd = {
			"id": 1,
			"title": "Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops",
			"price": 10995,
			"description": "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
			"category": "men's clothing",
			"image": "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
			"rating": {
				"rate": 3.9,
				"count": 120
			}
		}

		expect(reducer(previousState, addElement(productToAdd))).toEqual({
			value: [{ quantity: 1, product: productToAdd }]
		})
	})

	it('should handle a product being added to an existing list', () => {
		const productToAdd = {
			"id": 1,
			"title": "Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops",
			"price": 10995,
			"description": "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
			"category": "men's clothing",
			"image": "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
			"rating": {
				"rate": 3.9,
				"count": 120
			}
		}

		const previousState = {
			value: [{ quantity: 1, product: productToAdd }],
		}

		expect(reducer(previousState, addElement(productToAdd))).toEqual({
			value: [{ quantity: 2, product: productToAdd }]
		})
	})

	it('should handle a product\'s quantity being changed to an existing list', () => {
		const productToAdd = {
			"id": 1,
			"title": "Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops",
			"price": 10995,
			"description": "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
			"category": "men's clothing",
			"image": "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
			"rating": {
				"rate": 3.9,
				"count": 120
			}
		}
		const productChange = { quantity: 10, product: productToAdd }

		const previousState = {
			value: [{ quantity: 1, product: productToAdd }],
		}

		expect(reducer(previousState, changeQuantity(productChange))).toEqual({
			value: [{ quantity: 10, product: productToAdd }]
		})
	})

	it('should handle a product being removed from an existing list', () => {
		const productToAdd = {
			"id": 1,
			"title": "Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops",
			"price": 10995,
			"description": "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
			"category": "men's clothing",
			"image": "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
			"rating": {
				"rate": 3.9,
				"count": 120
			}
		}

		const previousState = {
			value: [{ quantity: 1, product: productToAdd }],
		}

		expect(reducer(previousState, removeElement(productToAdd))).toEqual({
			value: []
		})
	})

})
