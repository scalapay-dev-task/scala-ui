import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './pages/home';
import Checkout from './pages/checkout';
import reportWebVitals from './reportWebVitals';
import { store } from './store'
import { Provider } from 'react-redux'
import {
	BrowserRouter,
	Routes,
	Route
} from "react-router-dom";
import Navbar from './components/navbar';
import Footer from './components/footer';
import CanceledOrder from './components/canceledOrder';
import SuccessOrder from './components/successOrder';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
			<BrowserRouter>
				<div className="h-screen flex flex-col">
					<Navbar />
					<Routes>
						<Route path="/" element={<Home />} />
						<Route path="/checkout" element={<Checkout />} />
						<Route path="/order/cancel" element={<CanceledOrder />} />
						<Route path="/order/success" element={<SuccessOrder />} />
					</Routes>
					<Footer />
				</div>
			</BrowserRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
