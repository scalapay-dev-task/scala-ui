import { useEffect, useState } from 'react';

export const numberFormat = (value) =>
	new Intl.NumberFormat('it-IT', {
		style: 'currency',
		currency: 'EUR'
	}).format(value);

export const handleErrors = (response) => {
	if (!response.ok) throw new Error(response.status);
	return response;
}

export const convertBasketToScalaPayModel = (basket) => {
	return basket.map(el => ({
		price: { amount: String(el.product.price / 100), currency: "EUR" },
		name: el.product.title,
		category: el.product.category,
		quantity: el.quantity,
		sku: el.product.id.toString()
	}))
}

export const trimString = (value, length = 45) => {
	return value.length > length ?
		value.substring(0, length - 3) + "..." :
		value;
}

export const useMatchMedia = (mediaQuery, initialValue) => {
	const [isMatching, setIsMatching] = useState(initialValue)
	useEffect(() => {
		const watcher = window.matchMedia(mediaQuery)
		setIsMatching(watcher.matches)
		const listener = (matches) => {
			setIsMatching(matches.matches)
		}
		if (watcher.addEventListener) {
			watcher.addEventListener('change', listener)
		} else {
			watcher.addListener(listener)
		}
		return () => {
			if (watcher.removeEventListener) {
				return watcher.removeEventListener('change', listener)
			} else {
				return watcher.removeListener(listener)
			}
		}
	}, [mediaQuery])

	return isMatching
}
