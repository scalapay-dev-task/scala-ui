import React from 'react';
import StarIcon from '../static/star.svg';
import { useDispatch, useSelector } from 'react-redux'
import { addElement, removeElement } from '../entities/basket'
import { FaTrash } from 'react-icons/fa';
import { numberFormat } from '../utils';

export default function ProductItem({ item }) {
	const dispatch = useDispatch()

	const [ storeProduct ] = useSelector((state) => state.basket.value)
		.filter(el => el.product.id === item.id);

	return (
		<div className="max-w-sm rounded overflow-hidden shadow-lg flex flex-col">
			<img className="object-contain h-48 w-full my-6" src={item.image} alt="product image" loading='lazy' />

			<div className="px-5 h-full grid grid-cols-1 content-around">
				<div className="font-bold text-xl text-[#808080] mb-2 text-center">{item.title}</div>

				<div className="p-4 flex items-center text-sm text-gray-600">
					{
						[...Array(Math.round(item.rating.rate)).keys()]
							.map(el => <img className="h-4 w-4 fill-current text-yellow-500" src={StarIcon} key={el} alt="star" /> )
					} <span className="ml-2">{item.rating.count} reviews</span>
				</div>

				<p className="text-black-600 text-3xl font-bold text-center">
					{numberFormat(item.price / 100)}
				</p>

				<div className="w-full text-center mb-4 pt-3 bottom-0 inline-flex">
					<button onClick={() => dispatch(addElement(item))} className="w-full bg-purple-600 hover:bg-purple-800 text-white font-bold py-2 px-4 rounded">
						Add to cart { storeProduct && `(${storeProduct.quantity})` }
					</button>

					{
						(storeProduct && storeProduct.quantity > 0) &&
						<button onClick={() => dispatch(removeElement(item))} className="ml-5 px-3 border-2 border-[#FF0000] rounded">
							<FaTrash color="red" />
						</button>
					}
				</div>
			</div>
		</div>
	);
}
