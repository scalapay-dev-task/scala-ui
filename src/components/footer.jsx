import React from 'react';

export default function Footer() {
	return (
		<footer className="text-center lg:text-left bg-gray-100 text-gray-600 w-full mt-auto">
			<div className="text-center p-6 bg-gray-200">
				<span>© {new Date().getFullYear()} Copyright: </span>
				<a className="text-gray-600 font-semibold" href="http://localhost:3000/">SCALA SHOP</a>
			</div>
		</footer>
	)
}

