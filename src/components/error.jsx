import React from 'react';
import ErrorIcon from '../static/error.svg';

export default function Error() {
	return (
		<div className="flex flex-col h-screen">
			<img className="h-64 m-5" src={ErrorIcon} alt='error image' />
			<h1 className="m-5 text-center font-bold text-xl">Oops... an error has occured</h1>
		</div>
	);
}
