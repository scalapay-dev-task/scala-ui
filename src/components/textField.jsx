import React from 'react';

export default function TextField({ labelText, type = 'text', placeholder, id, onChange, value, error, className, ...rest })
{
	return (
		<div className={className}>
			<label className="block text-gray-700 text-sm font-bold mb-2" htmlFor={id}>
				{labelText}
			</label>
			<input
				{...rest}
				onChange={({ target: { value } }) => onChange(value)} value={value}
				className={`rounded w-full py-2 px-3 text-gray-700 leading-tight ${
					error
						? 'border-2 focus:outline-none focus:ring focus:ring-rose-200 focus:border-rose-500 border-rose-600'
						: 'shadow border appearance-none focus:outline-none focus:shadow-outline shadow'
				}`}
				id={id} type={type} placeholder={placeholder} />

			{error && <p className='text-xs pl-2 text-red-500 mb-4'>{error}</p>}
		</div>
	);
}
