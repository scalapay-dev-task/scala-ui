import { convertBasketToScalaPayModel, handleErrors, numberFormat } from '../utils';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import TextField from './textField';

export default function CheckoutSummary({ className })
{
	const basket = useSelector((state) => state.basket.value);

	const [shipping, setShipping] = useState(1000);
	const [totalBasket, setTotalBasket] = useState(0);
	const [allowCheckout, setAllowCheckout] = useState(false);
	const [customer, setCustomer] = useState({ name: "", surname: "" });

	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(false);

	useEffect(() => {
		setTotalBasket(basket.reduce((partialSum, el) => partialSum + (el.quantity * el.product.price), 0))
	}, [basket]);

	useEffect(() => {
		setAllowCheckout(customer.name && customer.surname && totalBasket > 0)
	}, [customer, totalBasket]);

	const checkoutRequest = () => {
		setIsLoading(true);
		setError(false);

		fetch('https://try.readme.io/https://staging.api.scalapay.com/v2/orders', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer qhtfs87hjnc12kkos'
			},
			body: JSON.stringify({
				"consumer": {
					"givenNames": customer.name,
					"surname": customer.surname
				},
				"items": convertBasketToScalaPayModel(basket),
				"merchant": {
					"redirectConfirmUrl": "http://localhost:3000/order/success",
					"redirectCancelUrl": "http://localhost:3000/order/cancel"
				},
				"shippingAmount": {
					"amount": String(shipping / 100),
					"currency": "EUR"
				},
				"totalAmount": {
					"amount": String((parseInt(totalBasket) + parseInt(shipping)) / 100),
					"currency": "EUR"
				}
			})
		})
		.then(handleErrors)
		.then(res => res.json())
		.then(data => window.location.replace(data.checkoutUrl))
		.catch(() => setError(true))
	}

	return (
		<div className={`px-8 py-10 bg-[#f6f6f6] ${className}`}>
			<h1 className="font-semibold text-2xl border-b pb-8">Order Summary</h1>

			<div className="mt-4">
				<label className="font-medium inline-block mb-3 text-sm uppercase">Shipping</label>
				<select className="block p-2 text-gray-600 w-full text-sm" value={shipping} onChange={({ target: { value } }) => setShipping(value)}>
					<option value={1000}>Standard shipping - €10,00</option>
					<option value={2500}>Express shipping - €25,00</option>
				</select>
			</div>

			<div className="border-t mt-8 pt-8">
				<label className="font-medium inline-block mb-3 text-sm uppercase">Customer details</label>
				<TextField error={!customer.name ? 'Required field' : ''} placeholder="Name" id="name" value={customer.name} onChange={(e) => setCustomer({ ...customer, name: e })} />
				<TextField error={!customer.surname ? 'Required field' : ''} placeholder="Surname" id="surname" value={customer.surname} onChange={(e) => setCustomer({ ...customer, surname: e })} />
			</div>

			<div className="border-t mt-8">
				<div className="flex font-semibold justify-between py-6 text-sm uppercase">
					<span>Items total</span>
					<span>{numberFormat(totalBasket/100)}</span>
				</div>

				<div className="flex font-semibold justify-between py-6 text-sm uppercase">
					<span>Total cost</span>
					<span>{numberFormat((parseInt(totalBasket) + parseInt(shipping)) / 100)}</span>
				</div>

				<button
					type="button" onClick={() => checkoutRequest()}
					className={`rounded leading-6 text-sm inline-flex items-center px-4 py-2 bg-indigo-500 font-semibold hover:bg-indigo-600 py-3 text-sm text-white uppercase w-full disabled:bg-indigo-200 ${
						isLoading && 'transition ease-in-out duration-150 cursor-not-allowed'
					}`}
					disabled={!allowCheckout || isLoading}>
					{ isLoading && (
						<>
							<svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none"
									 viewBox="0 0 24 24">
								<circle className="opacity-25" cx="12" cy="12" r="10" stroke="currentColor" stroke-width="4"></circle>
								<path className="opacity-75" fill="currentColor"
											d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
							</svg>

							Processing...
						</>
					)}

					{ (!isLoading && !error) && 'Checkout' }
				</button>
			</div>
		</div>
	)
}
