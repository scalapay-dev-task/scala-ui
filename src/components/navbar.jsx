import React from "react";
import { FaShoppingCart } from 'react-icons/fa';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

export default function Navbar()
{
	const navigate = useNavigate();
	const productsQuantity = useSelector((state) => state.basket.value)
		.reduce((partialSum, el) => partialSum + el.quantity, 0);

	return (
		<nav className="bg-gray-800">
			<div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
				<div className="flex items-center justify-between h-16">
					<div className="flex items-center">
						<div className="flex-shrink-0">
							<img
								className="h-8 w-8"
								src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
								alt="Workflow"
							/>
						</div>

						<div className="hidden md:block cursor-pointer" onClick={() => navigate("/")}>
							<div className="ml-10 flex items-baseline space-x-4">
								<p className="text-white px-3 py-2 rounded-md text-md font-medium"> SCALA SHOP </p>
							</div>
						</div>
					</div>

					<span className="relative inline-block cursor-pointer" onClick={() => navigate("/checkout")}>
							<FaShoppingCart color='white' size={24} />
							<span className="absolute bottom-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">
								{productsQuantity}
							</span>
						</span>
				</div>
			</div>
		</nav>
	);
}
