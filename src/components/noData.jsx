import React from 'react';
import NoDataIcon from '../static/noData.svg';

export default function NoData() {
	return (
		<div className="flex flex-col h-screen">
			<img className="h-64 m-5" src={NoDataIcon} alt='error image' />
			<h1 className="m-5 text-center font-bold text-xl">No results found</h1>
		</div>
	);
}
