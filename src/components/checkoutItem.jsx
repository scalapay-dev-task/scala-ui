import React, { useEffect, useState } from 'react';
import { removeElement, changeQuantity } from '../entities/basket';
import TextField from './textField';
import { numberFormat, trimString } from '../utils';
import { useDispatch } from 'react-redux';

export default function CheckoutItem({ item }) {
	const dispatch = useDispatch();
	const [quantity, setQuantity] = useState(item.quantity || 0);

	useEffect(() => {
		dispatch(changeQuantity({ product: item.product, quantity: parseInt(quantity || 0) }));
	}, [quantity]);

	return (
		<div className="border-t mt-8 pt-8 flex w-full">
			<div className="w-20">
				<img className="h-24" src={item.product.image} alt="" />
			</div>

			<div className="flex flex-col justify-between ml-4 flex-grow">
				<span className="font-bold text-sm">{trimString(item.product.title)}</span>
				<span className="px-4 py-1 rounded-full text-gray-500 bg-gray-200 font-semibold text-sm flex align-center w-max cursor-pointer active:bg-gray-300 transition duration-300 ease">
					{item.product.category}
				</span>

				<a onClick={() => dispatch(removeElement(item.product))} className="font-semibold hover:text-red-500 text-gray-500 text-xs cursor-pointer">
					Remove
				</a>
			</div>

			<TextField className="text-center w-1/6" id="qty" value={quantity} onChange={setQuantity} type="number" min="1" error={quantity < 1 ? 'Invalid value' : ''} />

			<span className="text-center w-1/5 font-semibold text-sm">{numberFormat(item.product.price / 100)}</span>
			<span className="text-center w-1/5 font-semibold text-sm">{numberFormat((item.product.price / 100) * quantity)}</span>
		</div>
	)
}
