import React from 'react';
import CancelIcon from '../static/cancel.svg';

export default function CanceledOrder() {
	return (
		<div className="flex flex-col h-screen">
			<img className="h-64 m-5" src={CancelIcon} alt='error image' />
			<h1 className="m-5 text-center font-bold text-xl">Your order has been canceled successfully</h1>
		</div>
	);
}
