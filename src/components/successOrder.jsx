import React from 'react';
import SuccessIcon from '../static/orderConfirmed.svg';
import { useSearchParams } from 'react-router-dom';

export default function SuccessOrder() {
	const [searchParams, _] = useSearchParams();

	return (
		<div className="flex flex-col h-screen">
			<img className="h-64 m-5" src={SuccessIcon} alt='error image' />
			<h1 className="m-5 text-center font-bold text-xl">Your order has been confirmed successfully!</h1>
			<h4 className="m-5 text-center font-semibold italic text-lg">Order Number #{searchParams.get("orderToken")}</h4>
		</div>
	);
}
