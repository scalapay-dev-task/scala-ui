import { useSelector } from 'react-redux';
import CheckoutItem from '../components/checkoutItem';
import CheckoutSummary from '../components/checkoutSummary';
import { useMatchMedia } from '../utils';

export default function Checkout()
{
	const basketProducts = useSelector((state) => state.basket.value);
	const productsQuantity = basketProducts.reduce((partialSum, el) => partialSum + el.quantity, 0);

	const isMobileResolution = useMatchMedia('(max-width:1280px)', true)

	return (
		<main>
			<div className="container mx-auto mt-10">
				{isMobileResolution && <CheckoutSummary className="w-full"/>}

				<div className="flex shadow-md my-10">
					<div className="w-full bg-white px-10 py-10">
						<div className="flex justify-between border-b pb-8">
							<h1 className="font-semibold text-2xl">Shopping Cart</h1>
							<h2 className="font-semibold text-2xl">{productsQuantity} Items</h2>
						</div>

						<div className="flex mt-10 mb-5">
							<h3 className="font-semibold text-gray-600 text-xs uppercase w-2/5">Product Details</h3>
							<h3 className="font-semibold text-center text-gray-600 text-xs uppercase w-1/5 text-center">Quantity</h3>
							<h3 className="font-semibold text-center text-gray-600 text-xs uppercase w-1/5 text-center">Price</h3>
							<h3 className="font-semibold text-center text-gray-600 text-xs uppercase w-1/5 text-center">Total</h3>
						</div>

						{ basketProducts.map(el => <CheckoutItem item={el} key={el.product.id} /> ) }
					</div>

					{!isMobileResolution && <CheckoutSummary className="w-1/4" />}
				</div>
			</div>
		</main>
	);
}
