import ProductItem from '../components/productItem';
import { useEffect, useState } from 'react';
import Error from '../components/error';
import SkeletonCard from '../components/skeletonCard';
import TextField from '../components/textField';
import NoData from '../components/noData';
import { handleErrors } from '../utils';

function Home()
{
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [query, setQuery] = useState('');

  const fetchData = () => {
    setIsLoading(true);
    setError(false);

    fetch(`http://localhost:4000/products?q=${query}`)
      .then(handleErrors)
      .then(res => res.json())
      .then(json => setProducts(json))
      .catch(() => setError(true))
      .finally(() => setIsLoading(false))
  };

  useEffect(() => {
    clearTimeout(window.timer);
    window.timer = setTimeout(() => { fetchData(); }, 300);
  }, [setProducts, query]);

  return (
    <main>
      <div className="max-w-7xl mx-auto py-6">
        { error && <Error /> }
        { !error && (
          <>
            <TextField id="query" placeholder="Search..." value={query} onChange={setQuery} />

            <div className="pt-5 grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-1 lg:gap-2 sm:gap-0 md:gap-0 justify-center">
              { isLoading && [...Array(8).keys()].map(el => <SkeletonCard key={el} /> ) }
              { (!isLoading && products.length > 0) && products.map(el => <ProductItem item={el} key={el.id} />) }
            </div>

            { (!isLoading && products.length === 0) && <NoData /> }
          </>
        ) }
      </div>
    </main>
  );
}

export default Home;
