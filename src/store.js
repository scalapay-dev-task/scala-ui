import { configureStore } from '@reduxjs/toolkit'
import basketReducer from './entities/basket'

export const store = configureStore({
	reducer: {
		basket: basketReducer
	},
})
