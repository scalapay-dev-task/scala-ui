import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	value: [],
}

export const basketSlice = createSlice({
	name: 'basket',
	initialState,
	reducers: {
		changeQuantity: (state, action) => {
			const index = state.value.findIndex(el => el.product.id === action.payload.product.id);

			if(index !== -1) {
				state.value[index].quantity = action.payload.quantity;
			}
		},

		addElement: (state, action) => {
			const index = state.value.findIndex(el => el.product.id === action.payload.id);

			if(index !== -1) {
				state.value[index].quantity++;
			} else {
				state.value.push({ quantity: 1, product: action.payload })
			}
		},

		removeElement: (state, action) => {
			state.value = state.value.filter(el => el.product.id !== action.payload.id)
		}
	}
})

export const { addElement, removeElement, changeQuantity } = basketSlice.actions

export default basketSlice.reducer
