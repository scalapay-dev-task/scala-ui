module.exports = {
	rootDir: '.',
	testMatch: ['**/__tests__/**/*.+(ts|tsx|js)']
}
